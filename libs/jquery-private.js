// and the 'jquery-private' module, in the
// jquery-private.js file:
define(['jquery'], function ($) {
    return $.noConflict(true);
});