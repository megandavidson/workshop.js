workshop.js
===========

This is the (nearly) bare repository that we're gonna use for the JS workshop.

Requirements
------------

* Node
* NPM

Getting Started
---------------

1. [Fork this repo](https://bitbucket.org/basekit/workshop.js/fork).
2. Clone your fork to your computer.
3. Get a webserver running. Either make an Apache vhost for this repo, or run a
   temporary server using a one-liner like the example below.

```bash
$ ruby -rwebrick -e'WEBrick::HTTPServer.new(:Port => 3000, :DocumentRoot => Dir.pwd).start'
```
